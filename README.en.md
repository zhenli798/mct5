# MCT

## 1 Introduction

### 1.1 Team Overview

The model checking team at XX University is primarily engaged in research on model verification techniques for Lustre and AADL.

### 1.2 Team Members

**🚩 Team Leader:** Big Lychee

**🧑‍💻 Members:**

1. Twistzz
2. Little Mo Bai
3. Murmur
4. KK

> To be continued
