学习一个语言的几个部分：

语法+语义+语言习惯+库+工具，往往前三项是最需要掌握的

# **数据类型：**

## 基本数据类型：

ocmal会自动推断类型，一般情况下不需要人工给出（当然只要你想，完全可以指定类型），例如：

```ocaml
utop # (1998:int);;
- : int = 1998
utop # (1998:bool);;
Error: This expression has type int but an expression was expected of type bool

(*字符串中的各个字符访问*)
utop # "abc".[0];;
- : char = 'a'
─( 20:28:21 )─< command 73 >─────────────────────────────────────{ counter: 0 }─
utop # "abc".[1];;
- : char = 'b'
─( 20:49:18 )─< command 74 >─────────────────────────────────────{ counter: 0 }─
utop # "abc".[2];;
- : char = 'c'
```

## 多态类型：

```ocaml
(*ocaml中自带的函数fst、snd都是具有多态类型的函数*)
utop # fst;;
- : 'a * 'b -> 'a = <fun>
(*读作alpha和beta*)

utop # let id x=x;;
val id : 'a -> 'a = <fun>
utop # let id_int':int -> int =id;;
val id_int' : int -> int = <fun>
```

## unit类型：

```ocaml
(*类似于其他语言中的void*)
utop # ();;
- : unit = ()
```

## 列表：

```ocaml
utop # [];;
- : 'a list = []
─( 10:38:42 )─< command 69 >─────────────────────────────────────{ counter: 0 }─
utop # [1];;
- : int list = [1]
utop # [[1;2];[2;3];[3;4]];;
- : int list list = [[1; 2]; [2; 3]; [3; 4]]
(*向列表中添加元素*)
utop # 1::2::3::[];;
- : int list = [1; 2; 3]
```

::和@

cons用于向列表头部添加元素

@用于连接两个列表

## varients：

type关键字，用法类似其他语言中的构造函数：

```ocaml
type day = Sun | Mon | Tue | Wed | Thu | Fri | Sat
let d = Tue

let int_of_day d =
  match d with
  | Sun -> 1
  | Mon -> 2
  | Tue -> 3
  | Wed -> 4
  | Thu -> 5
  | Fri -> 6
  | Sat -> 7
```

## Records：

```ocaml
type ptype = TNormal | TFire | TWater
type mon = {name : string; hp : int; ptype : ptype}

let c = {name = "Charmander"; hp = 39; ptype = TFire};;
(*访问记录字段，方式1*)
c.hp;;
(*访问记录字段，方式2*)
match c with {name = n; hp = h; ptype = t} -> h;;
(*访问记录字段，方式3*)
match c with {name; hp; ptype} -> hp;;
```

## Tuples:

```ocaml
(*三个元素的叫triple*)
(1, 2, 10)
(*两个元素的叫pair*)
(true, "Hello")
([1; 2; 3], (0.5, 'X'))
```

![image-20230613200933914](/home/lyh/.config/Typora/typora-user-images/image-20230613200933914.png)

![image-20230613203758664](/home/lyh/.config/Typora/typora-user-images/image-20230613203758664.png)

coin：variients

student：records

dessert：tuples

## 类型别名：

```ocaml
(*类型别名（Type Alias）：使用type关键字可以创建类型别名，将一个已存在的类型赋予一个新的名称。*)
type point = float * float
```



# **表达式：**

- if-else

字符串比较的是长度，不是字母的先后顺序

```ocaml
utop # if "bad" > "bool" then "y" else "n";;
- : string = "n"
```

if else语句丢失else分支时报错

```ocaml
utop # if true then "y";;
Error: This expression has type string but an expression was expected of type
         unit
       because it is in the result of a conditional with no else branch
```

# **定义：**

```ocaml
─( 15:52:58 )─< command 5 >──────────────────────────────────────{ counter: 0 }─
utop # let x=1;;
val x : int = 1
─( 15:53:17 )─< command 6 >──────────────────────────────────────{ counter: 0 }─
utop # let (y:int)=2;;
val y : int = 2
─( 16:12:52 )─< command 7 >──────────────────────────────────────{ counter: 0 }─
utop # let z:int=3;;
val z : int = 3
─( 16:13:09 )─< command 8 >──────────────────────────────────────{ counter: 0 }─
utop # x+y+z;;
- : int = 6
```

定义不能直接当做子表达式使用，但可以使用in关键字

```ocaml
utop # (let t=1)+1;;
Error: Syntax error: operator expected.

utop # let t=0 in t;;
- : int = 0
utop # let b=1 in 2*b;;
- : int = 2
utop # let x=5 in ((let x=6 in x))+x;;
- : int = 11
utop # let x=5 in (let x=6 in x);;
Line 1, characters 4-5:
Warning 26: unused variable x.
- : int = 6

utop # let e=1 in (let e=2 in e+1);;
Line 1, characters 4-5:
Warning 26: unused variable e.
- : int = 3
```

# **函数：**

## 匿名函数：

```ocaml
utop # (fun x->x+1);;
- : int -> int = <fun>
utop # (fun x->x+1) 1;;
- : int = 2

utop # (fun x y->(x+.y)/.2.);;
- : float -> float -> float = <fun>
utop # (fun x y->(x+.y)/.2.) 1. 4.;;
- : float = 2.5
```

## 给函数命名：

```ocaml
utop # let inc=fun x->x+1;;
val inc : int -> int = <fun>
utop # let inc x =x+1;;
val inc : int -> int = <fun>
utop # ave 1. 0.5;;
- : float = 0.75
```

## 递归函数：

```ocaml
(*fib*)
utop # let rec fact n=
if n=0 then 1
else n*fact(n-1);;
val fact : int -> int = <fun>
─( 21:19:46 )─< command 35 >─────────────────────────────────────{ counter: 0 }─
utop # fact 0;;
- : int = 1
─( 21:25:57 )─< command 36 >─────────────────────────────────────{ counter: 0 }─
utop # fact 1;;
- : int = 1
─( 21:26:15 )─< command 37 >─────────────────────────────────────{ counter: 0 }─
utop # fact 2;;
- : int = 2
─( 21:26:18 )─< command 38 >─────────────────────────────────────{ counter: 0 }─
utop # fact 3;;
- : int = 6
(*列表求和*)
utop # let rec sum lst =
  match lst with
  | [] -> 0
  | h :: t -> h + sum t;;
val sum : int list -> int = <fun>

utop # sum [1;2];;
sum <-- [1; 2]
sum <-- [2]
sum <-- []
sum --> 0
sum --> 2
sum --> 3
- : int = 3
```

## 函数的部分赋值：

```ocaml
utop # let add s t =s+t;;
val add : int -> int -> int = <fun>
─( 21:27:08 )─< command 41 >─────────────────────────────────────{ counter: 0 }─
utop # add 2 3;;
- : int = 5
─( 21:57:46 )─< command 42 >─────────────────────────────────────{ counter: 0 }─
utop # add 1;;
- : int -> int = <fun>
─( 21:57:54 )─< command 43 >─────────────────────────────────────{ counter: 0 }─
utop # (add 1) 9;;
- : int = 10
```

实际上多参函数只是ocaml中的语法糖

## **前缀运算符：**

```ocaml
utop # (+)1 2;;
- : int = 3
─( 22:09:30 )─< command 46 >─────────────────────────────────────{ counter: 0 }─
utop # ( *)1 2;;
Line 1, characters 2-4:
Warning 2: this is not the end of a comment.
- : int = 2
─( 22:15:33 )─< command 47 >─────────────────────────────────────{ counter: 0 }─
utop # ( * )1 2;;
- : int = 2
```

application 运算符：

```ocaml
let （@@）f x = f x

utop # square succ 2*10;;
Error: This function has type int -> int
       It is applied to too many arguments; maybe you forgot a `;'.
─( 10:04:32 )─< command 65 >─────────────────────────────────────{ counter: 0 }─
utop # square @@ succ 2*10;;
- : int = 900
```

reverse application 运算符（管道pipeline）：

```ocaml
let （1>）x f= f x

utop # 2 |> succ |> square;;
- : int = 9
```

## 难点：判断函数类型

```ocaml
(*if后面必须接bool类型*)
let f x = if x then x else x (*val f : bool -> bool = <fun>*)
let g x y = if y then x else x (*val g : 'a -> bool -> 'a = <fun>*)
let h x y z = if x then y else z (*val h : bool -> 'a -> 'a -> 'a = <fun>*)
let i x y z = if x then y else y (*val i : bool -> 'a -> 'b -> 'a = <fun>*)
```

- [ ] ## 难点：**尾递归**

## 模式匹配：

匹配最后一个参数时，可以使用语法糖简写

```ocaml
(*两个递归函数功能一致，都是对int列表中的元素求和*)
let rec sum lst =
  match lst with
  | [] -> 0
  | h :: t -> h + sum t
  
let rec sum = function
  | [] -> 0
  | h :: t -> h + sum t
```

with let：

```ocaml
let p = e1 in e2
```

with functions：

```
let f p1 ... pn = e1 in e2   (* function as part of let expression *)
let f p1 ... pn = e          (* function definition at toplevel *)
fun p1 ... pn -> e           (* anonymous function *)
```



# 输出：

```ocaml
let print_stat name num =
  Printf.printf "%s: %F\n%!" name num
```

函数的第一个参数`Printf.printf`是格式说明符，最后一个转换说明符`%`!表示刷新输出缓冲区。

```ocaml
(*将字符串作为string收集而不打印*)
let string_of_stat name num =
  Printf.sprintf "%s: %F" name num
```

# 调试：

## 打印输出：

```ocaml
let inc x = x + 1

let inc x =
  let () = print_int x in
  x + 1
```

## 跟踪：

```ocaml
let rec fib x = if x <= 1 then 1 else fib (x - 1) + fib (x - 2);;
#trace fib;;

utop # fib 2;;
fib <-- 2
fib <-- 0
fib --> 1
fib <-- 1
fib --> 1
fib --> 2
- : int = 2
```

## 防御性编程：

断言+抛出异常+

```ocaml
(** [random_int bound] is a random integer between 0 (inclusive)
    and [bound] (exclusive).  Requires: [bound] is greater than ocaml0
    and less than 2^30. *)
(* possibility 1 *)
let random_int bound =
  assert (bound > 0 && bound < 1 lsl 30);
  (* proceed with the implementation of the function *)

(* possibility 2 *)
let random_int bound =
  if not (bound > 0 && bound < 1 lsl 30)
  then invalid_arg "bound";
  (* proceed with the implementation of the function *)

(* possibility 3 *)
let random_int bound =
  if not (bound > 0 && bound < 1 lsl 30)
  then failwith "bound";
  (* proceed with the implementation of the function *)    
```

## 单元测试：

